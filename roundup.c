int round_em_up (int n, int *arr) {
  int i;
  int sum = 0;
  for(i=0;i<n;i++) {
    sum += arr[i];
  }
  return sum;
}
